const lista : string[] = ['uno', 'dos', 'tres'];
console.log(lista);

type Usuario = {
	edad: number,
	nombre: string,
}

const nuevoUsuario: Usuario | undefined = {
	edad: 3434,
	nombre: "",
}
const clase: Usuario[] = [
	nuevoUsuario
];

