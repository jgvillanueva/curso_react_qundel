import React, {useState} from 'react';
import './App.css';
import Toggle from "./components/Toggle/Toogle";
import 'bootstrap/dist/css/bootstrap.css';
import Formik from "./components/Formik/Formik";
import Controlled from "./components/Controlled/Controlled";
import UnControlled from "./components/UnControlled/UnControlled";

function App() {
    const [showControlled, setShowControlled] = useState<boolean>(true);
    const [showFormik, setShowFormik] = useState<boolean>(false);

    const handleTipoForm = (value: boolean) => {
        setShowControlled(value);
    }


    const handleFormik = (value: boolean) => {
        setShowFormik(value);
    }


    return (
        <div className="App">
            <Toggle
                label="Uso de formik"
                handleChange={handleFormik}
                opcion1="Formik"
                opcion2="normal"
                default={showFormik}
            />

            {
                showFormik ?
                    <Formik />
                    :
                    <div>
                        <Toggle
                            label="Tipo de formulario"
                            handleChange={handleTipoForm}
                            opcion1="Controlled"
                            opcion2="Uncontrolled"
                            default={showControlled}
                        />
                        {
                            showControlled ?
                                <Controlled />
                                :
                                <UnControlled />
                        }

                    </div>
            }
        </div>
    );
}

export default App;
