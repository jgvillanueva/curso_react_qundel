import {useFormik} from "formik";
import {Mensaje} from "../Controlled/Controlled";

interface ErroresValidacion {
	asunto?: string,
	mensaje?: string,
}

const Formik = () => {
	const validate = (values: Mensaje) => {
		const errores: ErroresValidacion = {};
		if (!values.asunto) {
			errores.asunto = 'El asunto es obligatorio';
		} else  if (values.asunto.length < 5) {
			errores.asunto = 'El asunto debe tener más de 5 caracteres';
		}
		if (!values.mensaje) {
			errores.mensaje = 'El mensaje es obligatorio';
		} else  if (values.mensaje.length < 15) {
			errores.mensaje = 'El mensaje debe tener más de 15 caracteres';
		}
		return errores;
	};

	const formik = useFormik({
		initialValues: {
			asunto: '',
			mensaje: '',
		},
		onSubmit: values => {
			console.log(values);
		},
		validate,
	});

	return (
		<div>
			<div className='card'>
				<div className="card-header">
					<h1>Formik</h1>
				</div>
				<div className="card-body">
					<form
						className='formulario'
						onSubmit={formik.handleSubmit}
					>
						<div className='form-group'>
							<label htmlFor="asunto">Asunto</label>
							<input
								type='text'
								className='form-control'
								name='asunto'
								value={formik.values.asunto}
								onChange={formik.handleChange}
								onBlur={formik.handleBlur}
							/>
						</div>
						{
							formik.touched.asunto && formik.errors.asunto ?
								<div className="alert alert-danger my-3" data-testid="errores">
									{formik.errors.asunto}
								</div> : null
						}
						<div className='form-group'>
							<label htmlFor="mensaje">Mensaje</label>
							<textarea
								className='form-control'
								name='mensaje'
								value={formik.values.mensaje}
								onChange={formik.handleChange}
								onBlur={formik.handleBlur}
							></textarea>
						</div>
						{
							formik.touched.mensaje && formik.errors.mensaje ?
								<div className="alert alert-danger my-3" data-testid="errores">
									{formik.errors.mensaje}
								</div> : null
						}
						<button
							className="btn btn-primary my-2"
							type="submit"
							disabled={!formik.isValid || !formik.dirty}
						>
							Enviar
						</button>
					</form>
				</div>
			</div>
		</div>
	)
}
export default Formik;
