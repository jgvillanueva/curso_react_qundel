import style from './toggle.module.scss';
import {useEffect, useState} from "react";

type IProps = {
	label: string;
	handleChange: (value: boolean) => void;
	opcion1: string;
	opcion2: string;
	default: boolean;
}

export default function Toggle (props: IProps) {
	const [toggle, setToggle] = useState<boolean>();

	const handleClick = () => {
		const newToggle = !toggle;
		setToggle(newToggle);
		props.handleChange(newToggle);
	}

	useEffect(() => {
		setToggle(props.default);
	}, [props.default]);

	const getClasses = (): string => {
		const colorClass =
			toggle ? 'btn-primary' : 'btn-success';
		return `btn mx-2 ${colorClass}`;
	}

	return (
		<div className={style.toggle}>
			<span>{props.label}</span>
			<button
				onClick={handleClick}
				className={getClasses()}
			>
				{
					toggle ?
						props.opcion2
						:
						props.opcion1
				}
			</button>
		</div>
	)
}
