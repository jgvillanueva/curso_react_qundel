import {FormEvent, useState} from "react";

export interface Mensaje {
	asunto: string;
	mensaje: string;
	user_id?: string;
}

enum Fields {
	asunto = 'asunto',
	mensaje = 'mensaje',
	user_id = 'user_id',
}

export interface ErroresMensaje {
	asunto: string;
	mensaje: string;
}

const Controlled = () => {
	const [mensaje, setMensaje] = useState<Mensaje>({
		asunto: '',
		mensaje: '',
		user_id: '12',
	});

	const [errores, setErrores] = useState<ErroresMensaje>({
		asunto: '',
		mensaje: '',
	})

	const [formValid, setFormValid] = useState(false);

	const validaCampo = (key: string, value: string) => {
		const newErrores: ErroresMensaje = {...errores};
		switch (key) {
			case Fields.asunto:
				newErrores[key] = value && value.length > 5 ? '' : 'El asunto debe tener más de 5 caracteres';
				break;
			case Fields.mensaje:
				newErrores[key] = value && value.length > 15 ? '' : 'El mensaje debe tener más de 5 caracteres';
				break;
		}
		setErrores(newErrores);

		validaForm();
	}

	const validaForm = () => {
		let valido = true;
		Object.keys(errores).forEach((campo) => {
			// @ts-ignore
			if (errores[campo] !== '') {
				valido = false;
			}
		});
		setFormValid(valido);
	}

	const handleChange = (event: FormEvent) => {
		const target: HTMLInputElement = event.target as HTMLInputElement;
		const newData: Mensaje = {...mensaje};
		const name = target.name as Fields;
		newData[name] = target.value;
		setMensaje(newData);
		validaCampo(name, target.value);
	}

	const handleSubmit = (event: FormEvent) => {
		event.preventDefault();
		console.log(mensaje);
	}

	return (
		<div>
			<div className='card'>
				<div className="card-header">
					<h1>Controlled</h1>
				</div>
				<div className="card-body">
					<form
						className='formulario'
						onSubmit={handleSubmit}
					>
						<div className='form-group'>
							<label htmlFor="asunto">Asunto</label>
							<input
								type='text'
								className='form-control'
								name={Fields.asunto}
								value={mensaje.asunto}
								onChange={handleChange}

							/>
						</div>
						{
							errores.asunto ?
								<div className="alert alert-danger my-3" data-testid="errores">
									{errores.asunto}
								</div> : null
						}

						<div className='form-group'>
							<label htmlFor="mensaje">Mensaje</label>
							<textarea
									className='form-control'
									name={Fields.mensaje}
									value={mensaje.mensaje}
									onChange={handleChange}
								></textarea>
							</div>
						{
							errores.mensaje ?
								<div className="alert alert-danger my-3" data-testid="errores">
									{errores.mensaje}
								</div> : null
						}
							<div className='form-group'>
								<label htmlFor="asunto">User id</label>
								<select
									name={Fields.user_id}
									value={mensaje.user_id}
									onChange={handleChange}
								>
									<option value="11">Carmen</option>
									<option value="12">Claudia</option>
									<option value="13">Jorge</option>
									<option value="14">David</option>
								</select>
							</div>
							<button
								className="btn btn-primary my-2"
								type="submit"
								disabled={!formValid}
							>
								Enviar
							</button>
					</form>
				</div>
			</div>
		</div>
)
}
export default Controlled;
