import {fireEvent, render, screen, within} from "@testing-library/react";
import UnControlled from "./UnControlled";

const contenidoAsunto = 'Mi asunto de test';
const contenidoMensaje = 'Mi mensaje de test de más de 15';
describe('uncontrolled component', () => {
	it('render elements', () => {
		render(<UnControlled />);
		const asunto: HTMLInputElement = screen.getByPlaceholderText('escribe el asunto');
		expect(asunto).toBeInTheDocument();
		const mensaje: HTMLTextAreaElement = screen.getByPlaceholderText('escribe tu mensaje');
		expect(mensaje).toBeInTheDocument();

		const button: HTMLButtonElement = screen.getByRole('button');
		fireEvent.click(button);

		const errores = screen.getByTestId('errores');
		const errorAsunto = within(errores).getByText('El asunto debe tener más de 5 caracteres');
		expect(errorAsunto).toBeInTheDocument();
		const errorMensaje = within(errores).getByText('El mensaje debe tener más de 15 caracteres');
		expect(errorMensaje).toBeInTheDocument();

		asunto.value = contenidoAsunto;
		mensaje.value = contenidoMensaje;
		fireEvent.click(button);

		const errores2 = screen.queryByTestId('errores');
		expect(errores2).toBeNull();

		const form = screen.getByTestId('form');
		expect(form).toBeInTheDocument();
		expect(form).toHaveFormValues({
			asunto: contenidoAsunto,
			mensaje: contenidoMensaje,
		});

		expect(button.disabled).not.toBeTruthy();


	});
})
