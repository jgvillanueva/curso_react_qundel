import {FormEvent, useState} from "react";


const UnControlled = () => {

	const [errores, setErrores] = useState<string[]>([]);

	const validators: {[key: string]: Function} = {
		asunto: (value: string) => value && value.length > 5 ? '' : 'El asunto debe tener más de 5 caracteres',
		mensaje: (value: string) => value && value.length > 15 ? '' : 'El mensaje debe tener más de 15 caracteres',
	}

	const handleSubmit = (event: FormEvent<HTMLFormElement>) => {
		event.preventDefault();
		const form = event.target;
		const formData = new FormData(form as HTMLFormElement);
		const _errores: string[] = [];
		formData.forEach((value, key) => {
			const result = validators[key](value);
			if (result !== '') {
				_errores.push(result);
			}
		})
		setErrores(_errores);

		const formJson = Object.fromEntries(formData.entries());
		console.log(formJson)
	}

	const renderErrores = () => {
		if (errores.length === 0)
			return;

		return (
			<div className="alert alert-danger my-3" data-testid="errores">
				{
					errores.map((error, index) =>
						<div key={index}>{error}</div>
					)
				}
			</div>
		);

	}

	return (
		<div>
			<div className='card'>
				<div className="card-header">
					<h1>UnControlled</h1>
				</div>
				<div className="card-body">
					{renderErrores()}
					<form
						data-testid="form"
						className='formulario'
						onSubmit={handleSubmit}
					>
						<div className='form-group'>
							<label htmlFor="asunto">Asunto</label>
							<input
								type='text'
								className='form-control'
								name='asunto'
								placeholder="escribe el asunto"
							/>
						</div>
						<div className='form-group'>
							<label htmlFor="mensaje">Mensaje</label>
							<textarea
								className='form-control'
								name='mensaje'
								placeholder="escribe tu mensaje"
							></textarea>
						</div>
						<button
							className="btn btn-primary my-2"
							type="submit"
						>
							Enviar
						</button>
					</form>
				</div>
			</div>
		</div>
	)
}
export default UnControlled;
