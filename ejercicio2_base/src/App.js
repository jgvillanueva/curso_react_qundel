
import './App.css';
import Contador from "./components/Contador";

function App() {
  const titulo = "hola mundo"
  return (
    <div className="App">
     <h1> {titulo}</h1>
        <Contador titulo="Mi componente" />
    </div>
  );
}

export default App;
