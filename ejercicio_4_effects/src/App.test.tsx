import React from 'react';
import {fireEvent, render, screen, within} from '@testing-library/react';
import App from './App';
import {BrowserRouter} from "react-router-dom";

describe('App components', () => {
  beforeEach(() => {
    render(<BrowserRouter><App /></BrowserRouter>);
  })

  it('renders title', () => {
    const titleElement = screen.getByText(/Ejercicio 4/i);
    expect(titleElement).toBeInTheDocument();
    expect(titleElement).toBeInstanceOf(HTMLHeadingElement);

    const home = screen.getByTestId('home');
    expect(home).toBeInTheDocument();
    //const usuariosElement = screen.getByText(/Usuarios/i);
    //expect(usuariosElement).toBeInTheDocument();
  });


  it('comprobar que las rutas funcionan', () => {
    const linkLista = screen.getByText('Lista');
    fireEvent.click(linkLista);
    const contenedor = screen.getByTestId('contenedor-lista');
    expect(contenedor).toBeInTheDocument();
    const lista = within(contenedor).getByText('Lista');
    expect(lista).toBeInTheDocument();

    const linkUsuarios = screen.getByText('Usuarios');
    fireEvent.click(linkUsuarios);
    const contenedor2 = screen.queryByTestId('contenedor-lista');
    expect(contenedor2).not.toBeInTheDocument();
    const usuariostitle = screen.getByTestId('usuarioshome');
    expect(usuariostitle).toBeInTheDocument();
  });



})

