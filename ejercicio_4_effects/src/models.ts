
export type TUsuario = {
	id: number,
	nombre: string,
	edad: number,
}

export type TMensaje = {
	id: number,
	asunto: string,
	mensaje: string,
	user_id?: number,
}
