import React from 'react';
import './App.css';
import Lista from "./components/Lista";
import Usuarios from "./components/Usuarios";
import {TUsuario} from "./models";
import {createBrowserRouter, Link, Route, RouterProvider, Routes} from "react-router-dom";
import Home from "./components/Home";
import UsuariosHome from "./components/UsuariosHome";
import UsuariosPreferencias from "./components/UsuariosPreferencias";
import Notfound from "./components/Notfound";
import ListaDetalle from "./components/ListaDetalle";
import DetalleFetch from "./components/DetalleFetch";
import Filtro from "./components/Filtro";
import MensajesLoader from "./components/MensajesLoader";

function App() {
    const apiUrl: string = process.env.REACT_APP_API_URL || '';

    const listaUsuarios: TUsuario[] = [
        { id: 0, nombre: 'Juan', edad: 12},
        { id: 1, nombre: 'Julia', edad: 42},
        { id: 2, nombre: 'ana', edad: 34},
    ];

    const router = createBrowserRouter([
        {
            path: '/',
            element: <Home />
        },
        {
            path: '/usuarios',
            element: <Usuarios  listaUsuarios={listaUsuarios} />
        },
        {
            path: '/loader',
            element: <MensajesLoader />,
            loader: async () => {
                return await fetch(apiUrl)
                    .then(response => response.json());
            }
        },
        {
            path: '/lista',
            element: <Lista apiUrl={apiUrl} />,
            children: [
                {
                    path: 'detalle',
                    element: <ListaDetalle />
                },
            ]
        },

    ]);

    return (
        <div className="App">
            <h1>Ejercicio 4</h1>

            {/*<RouterProvider router={router}></RouterProvider>*/}

            <nav>
                <ul>
                    <li>
                        <Link to="/" >Home</Link>
                    </li>
                    <li>
                        <Link to="/lista" >Lista</Link>
                    </li>
                    <li>
                        <Link to="/usuarios" >Usuarios</Link>
                    </li>
                </ul>
            </nav>

            <Routes>
                <Route path="/">
                    <Route index element={<Home />} />
                    <Route path="lista">
                        <Route path=""  element={
                            <div data-testid="contenedor-lista">
                                <Lista apiUrl={apiUrl}/>
                            </div>
                        }/>
                        <Route path="detalle" element={<ListaDetalle />} />
                        <Route path=":id" element={<DetalleFetch />} />
                        <Route path="filtrar" element={<Filtro />} />
                    </Route>
                    <Route path="usuarios" element={<UsuariosHome />}>
                        <Route path="lista" element={<Usuarios listaUsuarios={listaUsuarios} />} />
                        <Route path="preferencias" element={<UsuariosPreferencias />} />
                    </Route>
                    <Route path="*" element={<Notfound />} />
                </Route>
            </Routes>


        </div>
    );
}

export default App;
