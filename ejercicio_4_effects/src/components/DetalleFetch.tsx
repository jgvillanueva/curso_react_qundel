import {FC, useEffect, useState} from "react";
import {useParams} from "react-router-dom";
import {TMensaje} from "../models";

type IProps = {
}
const DetalleFetch: FC<IProps> = (props) => {
	const {id} = useParams();

	const mensajeInicial: TMensaje = {
		id: 0,
		asunto: '',
		mensaje: '',
	};
	const [mensaje, setMensaje] = useState(mensajeInicial);

	useEffect( () => {
		const getData = async () => {
			const url = 'https://dev.contanimacion.com/api_tablon/api/mensajes/get/'
			const response = await fetch(url + id, {method: 'GET'});
			console.log(response);
			const data = await response.json();
			setMensaje(data);
		}

		getData()
			.catch(console.error);

	}, [id]);


	return (
		<div data-testid="detalleFetch">
			<h2>Detalle</h2>
			<h1>{mensaje.asunto}</h1>
			<h2>{mensaje.mensaje}</h2>
		</div>
	)
}
export default DetalleFetch;
