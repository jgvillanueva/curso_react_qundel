import {FC, useEffect, useState} from "react";
import {useLocation} from "react-router-dom";
import {TMensaje} from "../models";

type IProps = {
}
const ListaDetalle: FC<IProps> = (props) => {
	const location = useLocation();
	const [mensaje, setMensaje] = useState<TMensaje>();

	useEffect(() => {
		setMensaje(location.state.datosMensaje);
	}, [location.state]);


	return (
		<div data-testid="home">
			<h1>Detalle del mensaje</h1>
			<h2>{mensaje?.asunto}</h2>
			<p>{mensaje?.mensaje}</p>
		</div>
	)
}
export default ListaDetalle;
