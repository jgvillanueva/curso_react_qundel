import {FC, ReactElement, useEffect, useRef, useState} from "react";
import {TMensaje} from "../models";
import Mensaje from "./Mensaje";
import {createSearchParams, NavigateOptions, useNavigate} from "react-router-dom";

type IProps = {
	apiUrl: string;
}
const Lista: FC<IProps> = (props): ReactElement => {
	const inputFilter = useRef<HTMLInputElement | null>(null);
	const navigate  = useNavigate();

	const [mensajes, setMensajes] = useState<TMensaje[]>([]);
	const [error, setError] = useState<string>('');

	useEffect(() => {
		fetch(props.apiUrl, {method: 'GET'})
			.then(response => response.json())
			.then((response) => {
				if (response.ok === false) {
					setError(response.statusText);
				} else {
					setMensajes(response as TMensaje[]);
					setError('');
				}
				console.log(response);
			})
			.catch(e => {
				setError(e.message);
			})
	}, [props.apiUrl]);


	const renderMensajes = () => mensajes.map(
		mensaje => <Mensaje key={mensaje.id} mensaje={mensaje} ><h1>Nodo hijo</h1></Mensaje>
	)

	let errorText
	if (error) {
		errorText = error;
	}

	const filtrar = () => {
		const input: HTMLInputElement = inputFilter.current as HTMLInputElement;
		const options: NavigateOptions = {
			state: {mensajesRaw: mensajes},
		}
		navigate({
			pathname: 'filtrar',
			search: createSearchParams({
				textoFiltro: input.value
			}).toString()
		}, options);
	}

	return (
		<div>
			<h2>Lista</h2>
			{ errorText }
			<div>
				<label htmlFor="filter" >filtrar por asunto</label>
				<input name="filter" ref={inputFilter}/>
				<button onClick={filtrar}>Filtrar</button>
			</div>
			<ul>
				{renderMensajes()}
			</ul>
		</div>
	)
}

export default Lista;
