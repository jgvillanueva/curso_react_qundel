
import {FC, useEffect, useState} from "react";
import {useLocation, useSearchParams} from "react-router-dom";
import Mensaje from "./Mensaje";
import {TMensaje} from "../models";

type IProps = {
}
const Filtro: FC<IProps> = (props) => {
	const location = useLocation();
	const mensajesRaw = location.state.mensajesRaw as TMensaje[];

	const [searchParams] = useSearchParams();
	const textoFiltro = searchParams.get('textoFiltro');

	const [mensajes, setMensajes] = useState<TMensaje[]>();

	useEffect(() => {
		if (!textoFiltro || !mensajesRaw)
			return;
		console.log('texto a filtrar', textoFiltro);
		const mensajesFiltrados = mensajesRaw.filter(mensaje => mensaje.asunto.toLowerCase().includes(textoFiltro.toLowerCase()));
		setMensajes(mensajesFiltrados);
	}, [textoFiltro, mensajesRaw]);

	if(!mensajes) {
		return <div><h1>No hay mensajes</h1></div>
	}

	const renderMensajes = () => mensajes.map(
		(mensaje) => <Mensaje key={mensaje.id} mensaje={mensaje} ></Mensaje>
	)

	return (
		<div data-testid="home">
			<h1>Filtro de mensajes</h1>

			{ renderMensajes() }
		</div>
	)
}
export default Filtro;
