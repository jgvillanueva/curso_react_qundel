import {TMensaje} from "../models";
import {FC, ReactElement} from "react";
import {Link, NavigateOptions, useNavigate} from "react-router-dom";

type IProps = {
	mensaje: TMensaje;
	children?: ReactElement;
}
const Mensaje: FC<IProps> = (props) => {
	const navigate = useNavigate();
	const irDetalleStado = () => {
		const options: NavigateOptions = {
			state: {
				datosMensaje: props.mensaje,
			}
		};
		navigate('/lista/detalle', options);
	};

	return (
		<li data-testid="mensaje">
			<h3>{props.mensaje.asunto}</h3>
			<p>{props.mensaje.mensaje}</p>
			<button onClick={irDetalleStado}>Ver detalle (estado)</button>
			<Link to="detalle" state={{
				datosMensaje: props.mensaje,
			}}>Link a detalle</Link>
			<Link to={props.mensaje.id.toString()}>Link con id</Link>
			{props.children}
		</li>
	)
}
export default Mensaje;
