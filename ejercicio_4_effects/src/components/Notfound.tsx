
import {FC} from "react";

type IProps = {
}
const Notfound: FC<IProps> = (props) => {
	return (
		<div data-testid="home">
			<h1>404 not found</h1>
		</div>
	)
}
export default Notfound;
