import {FC, ReactElement} from "react";
import {TUsuario} from "../models";

type IProps = {
	listaUsuarios: TUsuario[],
}
const Usuarios: FC<IProps> = (props): ReactElement => {


	const renderUsuarios = () =>
		props.listaUsuarios
			.map(usuario =>
				<li key={usuario.id} data-testid="usuario">{usuario.nombre}</li>
			)

	return (
		<div data-testid="usuarios">
			<h2>Usuarios</h2>
			<ul>
				{ renderUsuarios() }
			</ul>
		</div>
	)
}

export default Usuarios;
