import {render, screen} from "@testing-library/react";
import Lista from "./Lista";
import fetchMock from "fetch-mock";
import {BrowserRouter} from "react-router-dom";

describe('Lista component', () => {
	const mensajes = [
		{
			"id": 81,
			"asunto": "xxxxxxxxxxxxx",
			"mensaje": "xxxxxxxx",
			"name": "Cristina",
			"user_id": 15,
			"created_at": "2020-08-29 18:56:26"
		},
		{
			"id": 82,
			"asunto": "Prueba OC",
			"mensaje": "Hello hello OC",
			"name": "Jorge",
			"user_id": 13,
			"created_at": "2020-09-02 01:19:57"
		},
		{
			"id": 83,
			"asunto": "Hola",
			"mensaje": "yo soy Cristina",
			"name": "Jorge",
			"user_id": 13,
			"created_at": "2020-09-29 06:34:29"
		},
		{
			"id": 84,
			"asunto": "aaaadasdsa",
			"mensaje": "asdsadssada",
			"name": "Jorge",
			"user_id": 13,
			"created_at": "2020-09-29 15:03:41"
		},
	];
	const url = 'http://mensajesfake';

	beforeEach(() => {
		fetchMock.get(url, mensajes);
	})
	afterEach(() => {
		fetchMock.reset();
	});


	it('render elements', async () => {
		render(<BrowserRouter><Lista apiUrl="http://mensajesfake" /></BrowserRouter>);
		const listaMensajes = await screen.findAllByTestId('mensaje');
		expect(listaMensajes.length).toEqual(mensajes.length);
	});

	it('show error', async () => {
		fetchMock.catch(new Response('not found', {status: 404, statusText: 'not found'}));
		render(<BrowserRouter><Lista apiUrl="http://mensajesfake/noExiste" /></BrowserRouter>);
		const error = await screen.findByText(/not found/i);
		expect(error).toBeInTheDocument();
	});

	it('render once', async () => {
		fetchMock.once('http://mensajesfake2', mensajes)
		render(<BrowserRouter><Lista apiUrl="http://mensajesfake2" /></BrowserRouter>);

		const listaMensajes = await screen.findAllByTestId('mensaje');
		expect(listaMensajes.length).toEqual(mensajes.length);
	});
})

