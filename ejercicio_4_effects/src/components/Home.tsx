
import {FC} from "react";

type IProps = {
}
const Home: FC<IProps> = (props) => {
	return (
		<div data-testid="home">
			<h1>Bienvenido al curso react</h1>
		</div>
	)
}
export default Home;
