import {TUsuario} from "../models";
import {render, screen} from "@testing-library/react";
import Usuarios from "./Usuarios";

describe('Usuarios cmponent', () => {

	it('renders usuarios', () => {
		const listaUsuarios: TUsuario[] = [
			{id: 0, nombre: 'Carlos', edad: 88},
			{id: 2, nombre: 'Pepe', edad: 18},
		];

		render(<Usuarios listaUsuarios={listaUsuarios}></Usuarios>)

		const titleElement = screen.getByText(/Usuarios/i);
		expect(titleElement).toBeInTheDocument();
		const elemento = screen.getByText(listaUsuarios[0].nombre);
		expect(elemento).toBeInTheDocument()

		const items = screen.getAllByTestId('usuario');
		expect(items.length).toBe(listaUsuarios.length);
	});
})
