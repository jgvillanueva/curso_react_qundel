import {FC, useEffect, useState} from "react";
import {useLoaderData} from "react-router-dom";
import {TMensaje} from "../models";
import Mensaje from "./Mensaje";

type IProps = {
}
const MensajesLoader: FC<IProps> = (props) => {
	const [mensajes, setMensajes] = useState<TMensaje[]>([]);
	const loaderData: TMensaje[] = useLoaderData() as TMensaje[];

	useEffect(() => {
		setMensajes(loaderData);
	}, [loaderData, setMensajes]);

	const renderMensajes = () => mensajes.map(
		mensaje => <Mensaje key={mensaje.id} mensaje={mensaje} ><h1>Nodo hijo</h1></Mensaje>
	)

	return (
		<div data-testid="home">
			<h1>Bienvenido al curso react</h1>
			{renderMensajes()}
		</div>
	)
}
export default MensajesLoader;
