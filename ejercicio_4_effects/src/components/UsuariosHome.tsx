
import {FC} from "react";
import {Link, Outlet} from "react-router-dom";

type IProps = {
}
const UsuariosHome: FC<IProps> = (props) => {
	return (
		<div data-testid="usuarioshome">
			<h1>apartado usuarios</h1>
			<nav>
				<ul>
					<li>
						<Link to="lista">Lista de usuarios</Link>
					</li>
					<li>
						<Link to="preferencias">Preferencias</Link>
					</li>
				</ul>
			</nav>

			<Outlet />
		</div>


	)
}
export default UsuariosHome;
