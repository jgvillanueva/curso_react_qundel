import React, {FC, ReactElement} from "react";
import {Usuario} from "../lista/Lista.tsx";

type IProps = {
	usuario: Usuario | null
}

const Detalle: FC<IProps> = (props): ReactElement | null => {
	if (!props.usuario)
		return null;

	return(
		<h2 className="item">
			nombre: {props.usuario.nombre}
			edad: {props.usuario.edad}
		</h2>
	)
}

export default React.memo(Detalle);
