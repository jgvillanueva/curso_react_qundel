import {FC, ReactElement, useState} from "react";
import {Usuario} from "../lista/Lista.tsx";
import './item.scss';

type IProps = {
	usuario: Usuario
	handleVerGrande: (usuario: Usuario) => void
}

const Item: FC<IProps> = (props): ReactElement => {
	const usuario = {...props.usuario};
	const [valorToggle, setValorToggle] = useState<boolean>(true);

	const handleClick = (): void => {
		const toggle  = !valorToggle;
		setValorToggle(toggle);
	}

	return(
		<li className="item">
			nombre: {usuario.nombre}
			<button onClick={handleClick}>toggle</button>
			{
				valorToggle ?
					<>edad: {usuario.edad}</>
					:
					null
			}
			<button onClick={() => {props.handleVerGrande(usuario)}}>Ver grande</button>

		</li>
	)
}

export default Item;
