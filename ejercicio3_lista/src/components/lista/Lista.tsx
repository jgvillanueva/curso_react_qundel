import {FC, ReactElement, useState} from "react";
import Item from "../item/Item.tsx";
import Detalle from "../detalle/Detalle.tsx";
import styles from './lista.module.scss';

type IProps = {
	titulo: string,
}

export type Usuario = {
	nombre: string,
	edad: number,
}

const Lista: FC<IProps> = (props): ReactElement => {
	const usuarios: Usuario[] = [
		{nombre: "jorge", edad: 12},
		{nombre: "ana", edad: 32},
		{nombre: "paco", edad: 43},
		{nombre: "juan", edad: 44},
	];

	const renderUsuarios = (): ReactElement[] => {
		return usuarios.map((usuario, index) => {
			return <Item key={index} usuario={usuario} handleVerGrande={hacerGrande}></Item>
		})
	}

	const [usuario, setUsuario] =
		useState<Usuario | null>(null);

	const hacerGrande = (usuario: Usuario): void => {
		setUsuario(usuario);
	}

	return(
		<div className={styles.lista + " otro-estilo"}>
			<h1>{props.titulo}</h1>
			<Detalle usuario={usuario} />
			<ul>
				{ renderUsuarios() }
			</ul>
		</div>
	)
}

export default Lista;
