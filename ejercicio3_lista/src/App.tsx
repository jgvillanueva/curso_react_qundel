
import './App.css'
import Lista from "./components/lista/Lista.tsx";

function App() {

    return (
        <>
            <nav><ul><li>home</li><li>usuarios</li></ul></nav>
            <Lista titulo="Lista" />
        </>
    )
}

export default App
